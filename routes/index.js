const router = require('koa-router')()

router.get('/', async (ctx, next) => {
	ctx.body = {
		code: 200,
		msg: 'success'
	}
})

module.exports = router