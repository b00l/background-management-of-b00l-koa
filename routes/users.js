const router = require('koa-router')()
const sql = require('../utils/mysql_config')
const { createToken, analysisToken } = require('../utils/create_token')

router.prefix('/api/usr')

router.post('/login', async function (ctx, next) {

	const uname = ctx.request.body.name
	const passwd = ctx.request.body.passwd

	const res = await sql.findUserData(uname)

	if (res.length === 0) {
		ctx.body = {
			code: 403,
			msg: '用户名或密码错误'
		}
	} else {
		if (res[0].passwd === passwd) {
			ctx.body = {
				code: 200,
				msg: 'success',
				token: createToken({
					name: res[0].account,
					passwd: res[0].passwd
				})
			}
		} else {
			ctx.body = {
				code: 403,
				msg: '用户名或密码错误'
			}
		}
	}
})


module.exports = router