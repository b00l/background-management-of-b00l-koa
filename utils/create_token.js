const jwt = require('jsonwebtoken');

const PUBLIC_KEY = 'b00l';

let createToken = (payload = {}) => {
    try {
        let res = jwt.sign(payload, PUBLIC_KEY, {
            expiresIn: '4h'
        })
        return res
    } catch (err) {
        return 'again'
    }
}

exports.createToken = createToken

let analysisToken = (token = '') => {
    try {
        let res = jwt.verify(token, PUBLIC_KEY)
        console.log(res)
        return res
    } catch (err) {
        return 'again'
    }
}

exports.analysisToken = analysisToken