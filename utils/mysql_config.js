const mysql = require('mysql')

var pool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: 'geek',
    database: 'b00l_manager'
});

let allServices = {
    query: function (sql, values) {
        return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
                if (err) {
                    reject(err)
                } else {
                    connection.query(sql, values, (err, rows) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(rows)
                        }
                        connection.release()
                    })
                }
            })
        })
    },
    findUserData: function (name) {
        let _sql = `select * from users where account='${name}';`
        return allServices.query(_sql)
    },
    addUserData: (obj) => {
        let _sql = ``
        return allServices.query(_sql, obj)
    },
}

module.exports = allServices;